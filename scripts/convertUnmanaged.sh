#!/bin/bash
if [ -z "$1" ] ; then
    echo "Usage: $0 [orgName] [UnmanagedPackage/ChangeSet Name]" 1>&2
    exit 1
fi
orgNames="$1"

UPackage="$2"
if [ -z "$2" ] ; then
    UPackage="upackage"
fi

#echo Set the nmame of the org where your package/change set is?
#read orgName

#echo How is the name of your unmanagedPackage/ChangeSet?
#read changeSet

#echo It\'s nice to meet you $orgName

# create folder
echo Creating temp folder
mkdir temp

# create get the package
sfdx force:mdapi:retrieve -s -r ../temp -u $1 -p $2
unzip ../temp/unpackaged.zip -d ../temp/
sfdx force:mdapi:convert -r ../temp


echo You change set was converted into your project successfully. You can remove the temp folder!